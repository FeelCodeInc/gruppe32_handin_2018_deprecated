#Prosjekt i Ooprog - våren 2018

* Bemerkninger - [bemerkninger.md](document/bemerkninger.md)
* Kodestandard - [codestandard.md](document/codestandard.md)
* Kontaktinformasjon - [contact.md](document/contact.md)
* Referanseliste - [reference.md](document/reference.md)
* Progress report - https://hackmd.io/s/S1aMvBVPz
* Progress report edit - https://hackmd.io/KwFgRuIAwgtAJgQwBz1iYBjAbLRIQBGWbAMwFNhzSxhDlzCg <br>

