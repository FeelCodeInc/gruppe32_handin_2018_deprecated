## Referanseliste

### 2018-03-13
* string literal c++ - http://en.cppreference.com/w/cpp/language/string_literal
* rubular regex tool - http://rubular.com/
* Doxygen configuration format manual - https://www.stack.nl/~dimitri/doxygen/manual/config.html
* string literal ""s operator - http://en.cppreference.com/w/cpp/string/basic_string/operator%22%22s


### 2018-03-12

* compile with Visual Studio and CMake  - https://dmerej.info/blog/post/cmake-visual-studio-and-the-command-line/
* visual studio c++17 progress - https://blogs.msdn.microsoft.com/vcblog/2017/12/19/c17-progress-in-vs-2017-15-5-and-15-6/
* cpp compiler support - https://en.cppreference.com/w/cpp/compiler_support
